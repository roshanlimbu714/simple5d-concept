  
var content = document.querySelectorAll('section');

var idlePeriod = 100;
var animationDuration = 1000;
var index =0;
var lastAnimation = 0;
var scrollValue = 0;
setTimeout(()=>{
    content[0].classList.add('active-section');
},500)


function initiateAnimation(val){
    content.forEach(con=>con.classList.remove('active-section'));
    content[val].classList.add('active-section')
}
function openDialog(type,dialogEl){
    let dialog=document.querySelector('.'+type+'-dialog');
    dialog.classList.add('appear');
    dialog.querySelector('img').src= dialogEl.querySelector('img').src;
    console.log(dialog);
    console.log(type);
    dialog.querySelector('.title').innerText= dialogEl.querySelector('.'+type+'-title').innerText;
    dialog.querySelector('.description').innerText= dialogEl.querySelector('.d-none').innerText;
}

function closeDialog(type){
    document.querySelector('.'+type+'-dialog').classList.remove('appear');
}

function toggleNav(){
    document.querySelector('.nav-items').classList.toggle('appear');
    document.querySelector('#closeNav').classList.toggle('d-none');
}
window.addEventListener('keyup', function (event) {
    if (event.keyCode === 40) {
        console.log(event.keyCode);
        if (index === (content.length - 1)) return;
        index++;
        console.log(index);
    }

    if (event.keyCode === 38) {
        if (index < 1) return;
        index--;
    }
   switchClass(index);
},{passive:false});


window.addEventListener('wheel', function (event) {
    var delta = event.wheelDelta;
    console.log(delta);
    var timeNow = new Date().getTime();
    if (timeNow - lastAnimation < idlePeriod + animationDuration) {
        event.preventDefault();
        return;
    }
    // scrollDivs(delta);
    if (delta < 0) {
        if (index == (content.length - 1)) return;
        index++;

    } else {
        if (index < 1) return;
        index--;
    }
    switchClass(index);
    lastAnimation = timeNow;
},{passive:false});

function switchClass(a){
    scrollOnClick(a);
}

function scrollOnClick(a) {
    index = a;
    // console.log(index);
    scrollDivs();
}

function scrollDivs() {
    this.content.forEach(function (section, i){
        if (i === index) {
        section.scrollIntoView({behavior: 'smooth'});
        initiateAnimation(i);
        themeAdjustments(i);
    }
});
}


function swipeScroll(a) {
    if (a == content.length) return;
    if (a < 0) return;
    index = a;
    scrollDivs();
}



function toggleMenu(){
    document.getElementById('miniMenuBar').classList.toggle('active')
}



function changeIndex(i){
    // console.log(i);
    index=i;
}

function themeAdjustments(i){
     if(i===1){
        document.querySelector('.menubar').style.color='white';
        document.querySelector('.nav-item:nth-child(6)').classList.add('light-nav');
        document.querySelector('.nav-item:nth-child(5)').classList.add('light-nav');
     }else{

          document.querySelector('.nav-item:nth-child(6)').classList.remove('light-nav');
        document.querySelector('.nav-item:nth-child(5)').classList.remove('light-nav');
        document.querySelector('.menubar').style.color='initial';
     }
     if(i===2){
        document.querySelector('.logo').style.filter='invert(1) brightness(100)';
    }else{
        document.querySelector('.logo').style.filter='unset';
    }
}

function openDialog(type,dialogEl){
    let dialog=document.querySelector('.'+type+'-dialog');
    dialog.classList.add('appear');
    dialog.querySelector('img').src= dialogEl.querySelector('img').src;
    console.log(dialog);
    console.log(type);
    dialog.querySelector('.title').innerText= dialogEl.querySelector('.'+type+'-title').innerText;
    dialog.querySelector('.description').innerText= dialogEl.querySelector('.d-none').innerText;
}

function closeDialog(type){
    document.querySelector('.'+type+'-dialog').classList.remove('appear');
}

function toggleNav(){
    document.querySelector('.nav-items').classList.toggle('appear')
}
// swipe function
// var touchstartX = 0;
// var touchstartY = 0;
// var touchendX = 0;
// var touchendY = 0;

// var gesuredZone = document.querySelector('main');
// gesuredZone.addEventListener('touchstart', function(event) {
//     touchstartX = event.screenX;
//     touchstartY = event.screenY;
// }, false);

// gesuredZone.addEventListener('touchend', function(event) {
//     touchendX = event.screenX;
//     touchendY = event.screenY;
//     handleGesure();
// }, false); 

// function handleGesure() {
//     var swiped = 'swiped: ';
//     if (touchendX < touchstartX) {
//         // alert(swiped + 'left!');
//         if (index == (content.length - 1)) return;
//         index++;
//     }
//     if (touchendX > touchstartX) {
//          if (index < 1) return;
//         index--;
//         alert('swiped right')
//     }
//     if (touchendY < touchstartY) {
//          if (index < 1) return;
//         index--;
//     }
//     if (touchendY > touchstartY) {
//         if (index == (content.length - 1)) return;
//         index++;
//     }
//     // if (touchendY == touchstartY) {
//     //     break;
//     // }
//     switchClass(index);

// }

document.addEventListener('touchstart', handleTouchStart, false);        
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;                                                        
var yDown = null;

function getTouches(evt) {
  return evt.touches ||             // browser API
         evt.originalEvent.touches; // jQuery
}                                                     

function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];                                      
    xDown = firstTouch.clientX;                                      
    yDown = firstTouch.clientY;                                      
};                                                

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
        if ( xDiff > 0 ) {
            /* left swipe */ 
            if (index == (content.length - 1)) return;
            index++; 

        } else {
            /* right swipe */
            if (index < 1){ return};
            index--;
        }                       
    } else {
        evt.preventDefault();
        if ( yDiff > 0 ) { /* up swipe */ 
            // if (index == (content.length - 1)) return;
            // index++; 

        } else {  /* down swipe */
            // if (index < 1){ return};
            // index--;
        }                    
        return                                             
    }
    /* reset values */
    xDown = null;
    yDown = null;
    switchClass(index);
};

function changeScreen(mode){
    index=mode;
    changeActiveMode();
    scrollDivs();
}

function changeActiveMode(){
    let navItems=document.querySelectorAll('.nav-item');
    navItems.forEach(nav=>nav.classList.remove('active'));
    navItems[index].classList.add('active');
}